//Variables
const listaTweets = document.getElementById('lista-tweets');

//Event Listeners
eventListeners();

function eventListeners(){
	//Cuando se envia el formulario
	//tambien puedo usar.... document.querySelector('formulario').addEventListener('submit',agregarTweet); 
	document.getElementById('formulario').addEventListener('submit',agregarTweet);

	//Borrar Tweet
	listaTweets.addEventListener('click',borrarTweet);

	//Contenido Cargado
	document.addEventListener('DOMContentLoaded',localStorageListo);
}


//************* Funciones *************

//Agregar Twwet
function agregarTweet(e){
	e.preventDefault();

	//Leer el valor del Textarea
	//Tambien puedo usar...  const tweet = document.querySelector('tweet').value;  
	const tweet = document.getElementById('tweet').value;

	//Crear boton de Eliminar
	const botonBorrar = document.createElement('a');
	botonBorrar.classList = 'borrar-tweet';
	botonBorrar.innerText = 'X';

	//Crear elemento y agregarle el contenido a la lista
	const li = document.createElement('li');
	li.innerText = tweet;
	
	//Agrega el boton de borrar al tweet
	li.appendChild(botonBorrar);

	//Agrega el tweet a la lista
	listaTweets.appendChild(li);

	//Agrega el tweet a localStorage
	agregarTweetLocalStorage(tweet);
}


//Elimina el Tweet
function borrarTweet(e){
	e.preventDefault();AbortController
	if(e.target.className === 'borrar-tweet'){
		if(confirm("Seguro de Eliminar el Tweet") === true){
			e.target.parentElement.remove();
			borrarTweetLocalStorage(e.target.parentElement.innerText);
		}
	}
}


//Agrega Tweet a Local Storage
function agregarTweetLocalStorage(t){
	let tweets;
	tweets = obtenerTweetsLocalStorage();
	//Agregar al arreglo el nuevo tweet
	tweets.push(t);

	//Agregar a localStorage
	localStorage.setItem('tweets',JSON.stringify(tweets));
}


//Mostrar datos del LocalStorage en la Lista
function localStorageListo(){
	let tweets;
	tweets = obtenerTweetsLocalStorage();

	tweets.forEach(function(tweet){
		//Crear boton de Eliminar
		const botonBorrar = document.createElement('a');
		botonBorrar.classList = 'borrar-tweet';
		botonBorrar.innerText = 'X';

		//Crear elemento y agregarle el contenido a la lista
		const li = document.createElement('li');
		li.innerText = tweet;
		
		//Agrega el boton de borrar al tweet
		li.appendChild(botonBorrar);

		//Agrega el tweet a la lista
		listaTweets.appendChild(li);
	});
}


//Retorna lo que se tiene en LocalStorage
function obtenerTweetsLocalStorage(){
	let tweets
	//Revisando los valores de local Storage
	if(localStorage.getItem('tweets') === null){
		tweets = [];
	}else{
		tweets = JSON.parse(localStorage.getItem('tweets'));
	}
	return tweets;
}

//Eliminar Tweet del LocalStorage
function borrarTweetLocalStorage(t){
	let tweets, tweetBorrar;
	//Elimina la X del tweet
	tweetBorrar = t.substring(0,t.length - 1);
	tweets = obtenerTweetsLocalStorage();
	tweets.forEach(function(tweet,index){
		if(tweetBorrar === tweet){
			tweets.splice(index,1);
		}
	});

	//Agregar a localStorage
	localStorage.setItem('tweets',JSON.stringify(tweets));
}
